/**************************
* Stephen Mills
* CPSC 1021-003, F20
* smills3@clemson.edu
* Elliot McMillan & Victoria Xu
**************************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	Employee data[5];

	//get information from user
	for (int i = 0; i < 5; i++) {
		cout << "Enter first name: ";
		cin >> data[i].firstName;
		cout << "Enter last name: ";
		cin >> data[i].lastName;
		cout << "Enter birth year: ";
		cin >> data[i].birthYear;
		cout << "Enter hourly wage: ";
		cin >> data[i].hourlyWage;
		cout << endl;


	}


  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	 random_shuffle(&data[0], &data[5], myrandom);


   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
		Employee new_data[5] = {data[0], data[1], data[2], data[3], data[4]};


    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
		 sort(&new_data[0], &new_data[4], name_order);


    /*Now print the array below */
		for (auto i : new_data) {
			cout << setw(5) << i.lastName << ", " << i.firstName
			     << endl<< setw(10) << i.birthYear << endl << setw(10)
					 << i.hourlyWage << endl;
		}




  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {

  	if (lhs.lastName < rhs.lastName) {
			return (lhs.lastName < rhs.lastName);
		}

		return (lhs.lastName > rhs.lastName);
}
